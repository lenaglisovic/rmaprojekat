package com.example.rmaprojekat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MailActivity extends AppCompatActivity {

    private int indeks;
    private Mail m;
    private ArrayList<Mail> mailovi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);

        mailovi = (ArrayList<Mail>)getIntent().getSerializableExtra("mailovi");
        indeks = getIntent().getIntExtra("indeksi", -1);
        if(indeks == -1){
            addMail();
        }
        else{
            editMail();
        }
    }

    public void editMail() {
        EditText ime = findViewById(R.id.edittext_ime);
        EditText email = findViewById(R.id.edittext_email);
        Button prihvati = findViewById(R.id.button_prihvati);
        Button odustani = findViewById(R.id.button_odustani);

        m = mailovi.get(indeks);

        ime.setText(m.getIme());
        email.setText(m.getEmail());

        Intent i = new Intent(MailActivity.this, MainActivity.class);
        odustani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("mailovi", mailovi);
                startActivity(i);
            }
        });

        prihvati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mailovi.get(indeks).setIme(ime.getText().toString());
                mailovi.get(indeks).setEmail(email.getText().toString());
                i.putExtra("mailovi", mailovi);
                startActivity(i);
            }
        });

    }



    public void addMail() {
        EditText ime = findViewById(R.id.edittext_ime);
        EditText email = findViewById(R.id.edittext_email);
        Button prihvati = findViewById(R.id.button_prihvati);
        Button odustani = findViewById(R.id.button_odustani);

        m = new Mail();
        Intent i = new Intent(MailActivity.this, MainActivity.class);
        odustani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.putExtra("mailovi", mailovi);
                startActivity(i);
                finish();
            }
        });

        prihvati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ime.length() <= 0 || email.length() <= 0){
                    Toast.makeText(MailActivity.this, "Popuniti sva polja!", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(MailActivity.this, "Uspesno dodato!", Toast.LENGTH_SHORT).show();
                }

                m.setIme(ime.getText().toString());
                m.setEmail(email.getText().toString());
                mailovi.add(m);
                i.putExtra("mailovi", mailovi);

                startActivity(i);
                finish();

            }
        });

    }






}