package com.example.rmaprojekat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Context c;
    ArrayList<Mail> mailovi;
    ArrayList<Mail> shownMailovi;
    LinearLayout mainLayout;
    LinearLayout subLayout;
    ArrayList<LinearLayout> children;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        c = this;
        children = new ArrayList<>();
        mailovi = (ArrayList<Mail>)getIntent().getSerializableExtra("mailovi");
        if(mailovi == null)
            setupData();
            generateData();
            setupButton();
            setupEditTextSearch();

    }

    private void setupEditTextSearch() {
        EditText edittext_search = findViewById(R.id.edittext_search);
        edittext_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                shownMailovi = new ArrayList<Mail>();
                if(edittext_search.getText().equals("")){
                    shownMailovi = mailovi;
                }
                else{
                    for(Mail m : mailovi){
                        if(m.getIme().contains(edittext_search.getText().toString()) ||
                            m.getEmail().contains(edittext_search.getText().toString())){
                            shownMailovi.add(m);
                        }
                    }
                    clearAndGenerate();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void clearAndGenerate() {
        for(Mail m : mailovi){
            if(!shownMailovi.contains(m)){
                children.get(mailovi.indexOf(m)).setVisibility(View.GONE);
            }
            else{
                children.get(mailovi.indexOf(m)).setVisibility(View.VISIBLE);
            }
        }
    }

    private void setupButton() {
        Button button_add = findViewById(R.id.button_add);
        button_add.setOnClickListener((view) -> {
            Intent i = new Intent(MainActivity.this, MailActivity.class);
            i.putExtra("mailovi", mailovi);
            startActivity(i);
            finish();
        });
    }

    private void setupData() {
        mailovi = new ArrayList<Mail>();
        for(int i = 0; i < 5; i++){
            mailovi.add(new Mail("Ime" + i, "Email" + i));
        }
    }

    private void generateData() {
        LayoutInflater inflater = LayoutInflater.from(this);
        mainLayout = findViewById(R.id.mainLayout);

        LinearLayout mailContent;
        Button button_delete;
        TextView ime, email;
        ImageView slika;

        boolean color = false;
        for(final Mail m : mailovi){
            subLayout = (LinearLayout) inflater.inflate(R.layout.maillayout,
                    mainLayout, false);
            children.add(subLayout);


            ime = subLayout.findViewById(R.id.textview_ime);
            email = subLayout.findViewById(R.id.textview_email);
            slika = subLayout.findViewById(R.id.imageView);
            button_delete = subLayout.findViewById(R.id.button_delete);
            mailContent = subLayout.findViewById(R.id.mailContent);

            mailContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, MailActivity.class);
                    i.putExtra("indeks", mailovi.indexOf(m));
                    i.putExtra("mailovi", mailovi);
                    startActivity(i);
                    finish();
                }
            });

            button_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mainLayout.removeView(children.get(mailovi.indexOf(m)));
                    children.remove(mailovi.indexOf(m));
                    mailovi.remove(m);
                }
            });

            ime.setText(m.getIme());
            email.setText(m.getEmail());
            slika.setImageResource(R.drawable.mail);

            if(!color){
                color = true;
                subLayout.setBackgroundColor(Color.LTGRAY);
            }
            else{
                color = false;
                subLayout.setBackgroundColor(Color.GRAY);
            }
            mainLayout.addView(subLayout);
        }

        //Database db = new Database(this);

    }




}