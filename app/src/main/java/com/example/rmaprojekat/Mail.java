package com.example.rmaprojekat;

import java.io.Serializable;

public class Mail implements Serializable {

    //public static final String TABLE_NAME="mailing_lista";
    //public static final String FIELD_IME="ime";
    //public static final String FIELD_EMAIL="email";

    private String ime;
    private String email;


    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Mail(){

    }

    public Mail(String ime, String email){
        this.ime = ime;
        this.email = email;
    }



}
